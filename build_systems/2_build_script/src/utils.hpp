#pragma once
#include <ncursesw/ncurses.h>
#include "common/basicTypes.h"

#define VISUALIZATION_HELP_STR "Visualization defaults (can be changed in config.h):\n" \
"Enter - focus the currently selected person\n" \
"Arrow keys - move the camera\n" \
"h, j, k, l - move the selection\n" \
"i - display info about the selected person\n"

class AutoWin			// ncurses windows that is freed on destruction
{
public:
	explicit AutoWin(WINDOW*);
	~AutoWin();

	operator WINDOW*();
private:
	WINDOW* w_;
};

using fd_t = int;				/* file descriptor */
struct MappedFile
{
	char* data;
	unsigned len;
};

void init_ncurses();

MappedFile mapfile(fd_t);
fd_t openOrDie(const char* path);

/* wheather a pad with the given coordinates will be visible */
bool willBeVisible(I16 drawY, I16 drawX, U16 height, U16 width);

/* like pnoutrefresh but pminrow,col are calculated based on width,height and
   top left coords, which can be negative (outside the screen) */
void drawpad(WINDOW* pad, I16 drawY, I16 drawX);

I32 min(I32 a, I32 b);
I32 max(I32 a, I32 b);

template <class Vec>
void remove(Vec& vec, size_t pos) /* Remove from vector by moving the last element in its place */
{
	vec[pos] = vec[vec.size() - 1];
	vec.pop_back();
}
