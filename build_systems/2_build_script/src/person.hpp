#pragma once

#include <string>
#include <fstream>
#include <ncursesw/ncurses.h>
#include "common/basicTypes.h"

struct EventTime
{
	enum: I16 { UNKNOWN_DATE=0  };
	enum: I8  { UNKNOWN_TIME=-1 };

	EventTime(I16 Year=UNKNOWN_DATE, U8 Month=UNKNOWN_DATE, U16 Day=UNKNOWN_DATE, I8 Hour=UNKNOWN_TIME, I8 Minute=UNKNOWN_TIME);
	void print(WINDOW*) const;

	I16 year;		//The year in relation to Chritst's birth -  1 (AD) is the year He was born, -1 (BC) is the previous year. There is no year 0
	U8  month;
	U16 day;
	I8  hour;		//0-23
	I8  minute;		//0-59
};

enum Sex: U8
{
	M = 0,
	F
};

enum BoxColors: U8 {
	COLOR_MALE = 1,
	COLOR_FEMALE,
	COLOR_SELECTED
};

enum DisplayDimensons: U8 {
	BOX_HEIGHT = 5,
	LINK_HEIGHT = 3
};

class Person
{
public:
	Person() = default;
	Person(const char* aName, EventTime aBirth={}, Sex=M, EventTime aDeath={});
	/* Person(Person&&) = default; */
	/* Person& operator=(Person&&) = default; */
	/* Person& operator=(const Person&) = default; */

	bool operator==(const Person &other) const;

	void displayInfo() const;

	/* Draw a Person's box on the terminal
	   drawY,drawX - top left coordinates where to draw (negative if outside the screen)
	   hasSpouse, hasSpouse, hasChildren - wheather the person has them on display
	   returns the width of the box*/
	U16 draw(I16 drawY, I16 drawX, bool selected, bool hasSpouse,
			  bool hasParents, bool hasChildren=false, bool wholeName=true) const;

	std::string name;
	EventTime birth;
	Sex sex;
	EventTime death;
};
