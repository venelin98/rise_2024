#include "utils.hpp"
#include "person.hpp"
#include <clocale>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <iostream>

AutoWin::AutoWin(WINDOW* win)
	: w_(win)
{}

AutoWin::~AutoWin()
{
	delwin(w_);
}

AutoWin::operator WINDOW*()
{
	return w_;
}

void init_ncurses()
{
	setlocale(LC_ALL, "en_US.UTF-8");

	/* Init ncurses */
	initscr();			                /* start curses mode */
	atexit( (void(*)())endwin );		/* stop curses at exit */
	curs_set(0);		                /* Hide cursor */
	noecho();
	cbreak();	 /* get input instantly */
	keypad(stdscr, true);
	start_color();
	use_default_colors();
	/* init_pair(5, COLOR_BLACK, COLOR_BLACK); */
	/* bkgd(COLOR_PAIR(5)); */
	init_pair(COLOR_MALE, COLOR_CYAN, -1); /* -1 means use default */
	init_pair(COLOR_FEMALE, COLOR_RED, -1);
	init_pair(COLOR_SELECTED, COLOR_YELLOW, -1);
	wnoutrefresh(stdscr);					/* if not present the first real refresh is missed */
}

MappedFile mapfile(fd_t file)
{
	MappedFile res;
	res.len = lseek(file, 0, SEEK_END);
	res.data = (char*)mmap(nullptr, res.len, PROT_READ, MAP_PRIVATE, file, 0);

	close(file);
	return res;
}

fd_t openOrDie(const char* path)
{
	fd_t file = open(path, O_RDONLY);
	if(file == -1)
	{
		endwin();
		std::cerr << "Can't open: " << path << '\n';
		exit(1);
	}
	return file;
}


bool willBeVisible(I16 drawY, I16 drawX, U16 height, U16 width)
{
	return (drawY + height > 0) && (drawX + width > 0) &&
		drawY < LINES && drawX < COLS;
}

void drawpad(WINDOW* pad, I16 drawY, I16 drawX)
{
	const U16 pbegY = drawY>0 ? 0 : -drawY;		  /* pad top left coords to be displayed */
	const U16 pbegX = drawX>0 ? 0 : -drawX;

	pnoutrefresh(pad,
		     pbegY,     pbegX,
		     drawY,     drawX,
		     LINES - 1, COLS - 1);
}

I32 min(I32 a, I32 b)
{
	return a <= b ? a : b;
}

I32 max(I32 a, I32 b)
{
	return a >= b ? a : b;
}
