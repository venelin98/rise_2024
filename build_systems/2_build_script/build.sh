#!/bin/sh

language='
	-std=c++17
	-fno-rtti
	-fno-exceptions
	-fno-non-call-exceptions
	-fno-common'
# force each global variable to be only defined once

optimizations='
	-Ofast
	-flto
	-fipa-pta
	-march=native'

warnings='
	-Wall
	-Wextra
	-Wno-implicit-fallthrough
	-Wno-multichar'

g++ -o tft $language $optimizations $warnings -DNDEBUG -s src/*.cpp -lncursesw

chmod 775 tft
