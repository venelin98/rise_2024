#pragma once
#include <iostream>

#define ASSERT_EQ(a, b) if( a != b )							     \
	{								                     \
		std::cerr << "ASSERT failed at line " << __LINE__ << std::endl;              \
		std::cerr << #a << " == " << #b << std::endl;	                                 \
		std::cerr << "EXPECTED: " << (a) << std::endl << "ACTUAL: " << (b) << std::endl; \
		exit(1);                          						 \
	}
