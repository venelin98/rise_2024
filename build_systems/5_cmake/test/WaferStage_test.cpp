#include <iostream>
#include "test_utils.hpp"
#include "WaferStage.hpp"

using namespace std::chrono_literals;

void WaferStage_movement_test()
{
	WaferStage ws;

	ASSERT_EQ(ws.is_moving(), false);
	ASSERT_EQ(0.0f, ws.pos());

	// Start movement
	float acc = 0.0625f;
	ws.move_to(1.0f, acc);

	// Update each second and check distance
	ws.update(1s);
	ASSERT_EQ(acc, ws.pos());

	ws.update(1s);
	ASSERT_EQ(acc * 3, ws.pos());

	ws.update(1s);
	ASSERT_EQ(acc * 6, ws.pos());

	ws.update(1s);
	ASSERT_EQ(acc * 10, ws.pos());

	ws.update(1s);
	ASSERT_EQ(acc * 13, ws.pos());

	ws.update(1s);
	ASSERT_EQ(acc * 15, ws.pos());

	ws.update(1s);
	ASSERT_EQ(1.0f, ws.pos());
}

int main()
{
	WaferStage_movement_test();

	std::cout << "ALL TESTS PASSED!" << std::endl;
	return 0;
}
