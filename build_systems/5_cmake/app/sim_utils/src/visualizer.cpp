#include <iostream>
#include "visualizer.hpp"

enum
{
	TERM_DRAW_WIDTH = 100,
};

namespace visualizer
{
	void draw_in_term(const WaferStage& ws)
	{
		int pos_in_col = ws.pos() * TERM_DRAW_WIDTH;

		std::cout << '\r' << '|';

		for(int i = 0; i < pos_in_col-1; ++i)
			std::cout << ' ';

		std::cout << '@';

		for(int i = pos_in_col; i < TERM_DRAW_WIDTH; ++i)
			std::cout << ' ';

		std::cout << '|' << std::flush;
	}
}
