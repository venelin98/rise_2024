#include <iomanip>
#include "WsLog.hpp"
#include "WaferStage.hpp"

using namespace std::chrono;

WsLog::WsLog(const char* file_path)
	: file_(file_path)
{
	file_ << std::fixed << std::setprecision(2);
	start_ = system_clock::now();
}

void WsLog::log(const WaferStage& ws)
{
	auto ms = duration_cast<milliseconds>(system_clock::now() - start_).count();
	file_ << ms << ' ' << ws.pos() << std::endl;
}
