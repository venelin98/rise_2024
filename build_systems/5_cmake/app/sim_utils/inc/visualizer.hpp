// Visualize a WaferStage

#pragma once
#include "WaferStage.hpp"

namespace visualizer
{
	void draw_in_term(const WaferStage&);
}
