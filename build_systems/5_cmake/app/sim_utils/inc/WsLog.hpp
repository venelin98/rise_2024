// Log WaferStage positon to a file
#pragma once

#include <fstream>
#include <chrono>

class WaferStage;

class WsLog
{
public:
	explicit WsLog(const char* file_path);
	void log(const WaferStage&);

private:
	std::ofstream file_;
	std::chrono::time_point<std::chrono::system_clock> start_;
};
