// AUTHOR: Venelin Dechkov
// PRESUP: acceleration lasts as long as deceleration

#include <iostream>
#include <thread>
#include <chrono>

#include "WaferStage.hpp"
#include "visualizer.hpp"
#include "WsLog.hpp"

using namespace std::chrono_literals;

int main()
{
	WsLog logger("wafer_pos.log");
	WaferStage ws;

	do
	{
		if( !ws.is_moving() && std::cin)
		{
			int target_pos, acceleration; // [0; 100]
			std::cin >> target_pos >> acceleration;

			if( (target_pos >= 0 && target_pos <= 100) && (acceleration >=0 && acceleration <= 100) )
				ws.move_to(target_pos / 100.0f, acceleration / 100.0f);
		}

		ws.update(16ms); // 60 FPS
		visualizer::draw_in_term(ws);
		logger.log(ws);

		std::this_thread::sleep_for(16ms);
	}
	while(ws.is_moving() || std::cin);

	return 0;
}
