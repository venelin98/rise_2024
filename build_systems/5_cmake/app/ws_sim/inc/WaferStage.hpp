// Simulates wafer stage movement in real time

#pragma once
#include <chrono>

class WaferStage
{
public:
	// Update stage position, dur is time since last update
	void update(std::chrono::milliseconds dur);
	// Give a move request to the stage to be executed from now on
	void move_to(float target, float acceleration);

	float pos() const;
	bool is_moving() const;

private:
	// Wafer start, current and target positions [0, 1]
	float start_pos_ = 0;
	float pos_ = 0;
	float target_ = 0;

	float acc_ = 0; 	// acceleration per second
	float speed_ = 0;	// speed - positions units per second

	bool moving_ = false;
};
