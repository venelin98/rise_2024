#include <algorithm>
#include <cmath>
#include "WaferStage.hpp"

using std::fabs;

void WaferStage::update(std::chrono::milliseconds dur)
{
	if(!moving_)
		return;

	// Update acceleration
	float acc_delta = acc_ * dur.count() / 1000.0f;

	if( fabs(pos_ - start_pos_) < fabs(target_ - start_pos_)/2.0f )	// accelerating
	{
		speed_ += acc_delta;
	}
	else			// decelerating
	{
		speed_ = std::max(speed_ - acc_delta, 0.001f); // speed shouldnt reach 0 until we are there
	}

	// Update distance
	float dist_delta = speed_ * dur.count() / 1000.0f;

	if(pos_ < target_)
	{
		pos_ = std::min( target_, pos_ + dist_delta);
	}
	else
	{
		pos_ = std::max( target_, pos_ - dist_delta);
	}

	if(pos_ == target_)
		moving_ = false;
}

void WaferStage::move_to(float target, float acceleration)
{
	start_pos_ = pos_;
	target_ = target;

	acc_ = acceleration;
	speed_ = 0.0f;

	moving_ = true;
}

float WaferStage::pos() const
{
	return pos_;
}

bool WaferStage::is_moving() const
{
	return moving_;
}
