#pragma once

#include <vector>
#include "node.hpp"

class View
{
public:
    ~View();

    void addNode(Node* node);
    bool removeNode(const char* url);
    
    void show();
    void hide();
    void render();
    
protected:
    std::vector<Node*> nodes_;
};
