#include <iostream>
#include <string.h>
#include "view.hpp"

View::~View()
{
    for(auto& node: nodes_)
        delete node;
    
    std::cout << '\n';
}

void View::addNode(Node* node)
{
    nodes_.push_back(node);
}

bool View::removeNode(const char* url)
{
    bool found = false;
    for(size_t i=0; i<nodes_.size(); ++i)
    {
        if(!memcmp(nodes_[i]->getUrl(), url, 128))
        {
            delete nodes_[i];
            nodes_[i] = nodes_.back();
            nodes_.pop_back();
            found = true;
        }
    }
    return found;
}

void View::show()
{
    for(auto& node: nodes_)
        node->changeVisibility(true);
    
    std::cout << '\n';
}

void View::hide()
{
    for(auto& node: nodes_)
        node->changeVisibility(false);
    
    std::cout << '\n';
}

void View::render()
{
    for(auto& node: nodes_)
        node->draw();
    
    std::cout << '\n';
}
