#pragma once

class Node
{
public:
    explicit Node(char* url);
    virtual ~Node();
    
    const char* getUrl();
    void move(int X, int Y);

    virtual void changeVisibility(bool isVisible);
    virtual void draw();
    
protected:
    char* url_;
    int x=0; 
    int y=0;
};
