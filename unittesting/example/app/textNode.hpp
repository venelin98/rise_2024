#pragma once

#include <string>
#include "node.hpp"

class TextNode: public Node
{
public:
    using Node::Node;    //Inherit constructor
    virtual ~TextNode();
    
    virtual void changeVisibility(bool isVisible) override;
    virtual void draw() override;
    
    int getLineCount();
    int getLongestLineLen();
    
    void setText(const char* text);
    void setLineCount(int count);
    
protected:
    std::string text_;
};
