#include <iostream>
#include "textNode.hpp"

TextNode::~TextNode()
{
    std::cout << "TextNode with URL: " << url_ << " destroyed\n";
}

void TextNode::changeVisibility(bool isVisible)
{
    std::cout << "TextNode with URL: " << url_ << " is now " << (isVisible ? "visible" : "invisible") << '\n';
}

void TextNode::draw()
{
    std::cout << "Drew TextNode with URL: " << url_ << '\n';
}

int TextNode::getLineCount()
{
    int count = 0;
    for(auto c: text_)
    {
        count += c=='\n';
    }
    return count;
}

int TextNode::getLongestLineLen()
{
    int cur_len = 0;
    int max_len = 0;
    for(auto c: text_)
    {
        if(c=='\n')
        {
            if(cur_len > max_len)
                max_len = cur_len;
            cur_len = 0;
        }
        else
            ++cur_len;
    }
    return max_len;
}


void TextNode::setText(const char* text)
{
    text_ = text;
}

void TextNode::setLineCount(int count)
{
    int cur_count = getLineCount();
    int to_rm = cur_count - count;
    while(to_rm > 0)
    {
        for(int i=0; i<text_.size(); ++i)
        {
            if(text_[i]=='\n')
            {
                text_[i]=' ';
                --to_rm;
                break;
            }
        }
    }
}
