gcc - C/C++ compiler
g++ - gcc but treats files as C++
ar - archiver used to create *.a static library files from *.o object files

gcc options:
 -c - only compile, don't link (produce *.o object files)
 -o NAME - produce file called NAME as the output
 -l[LIBRARY] - link with library called lib[LIBRARY].so or lib[LIBRARY].a
 -L[DIR] - add [DIR] to library search path
 -O3 - enable most optimisations
 -Wall - enable most warnings
 -std=[STD] - compile using the language standart STD, for example c++11
 -I[DIR] - add [DIR] to the search path for included files with '#include'
 -fPIC - create position independant code, suitible for a shared object
